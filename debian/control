Source: gcc-sh-elf
Section: devel
Priority: optional
Maintainer: Debian Electronics Team <pkg-electronics-devel@alioth-lists.debian.net>
Uploaders: John Scott <jscott@posteo.net>
Build-Depends:
 binutils-sh-elf,
 bison,
 debhelper-compat (= 13),
 dejagnu <!nocheck>,
 flex,
 gawk,
 gcc-13-source,
 gcc-sh-elf <cross>,
 gdb-source,
 gettext,
 libisl-dev,
 libmpc-dev,
 libreadline-dev,
 libzstd-dev,
 newlib-source,
 texinfo,
 xz-utils,
 zlib1g-dev,
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/electronics-team/toolchains/gcc-sh-elf
Vcs-Git: https://salsa.debian.org/electronics-team/toolchains/gcc-sh-elf.git
Standards-Version: 4.6.2

Package: gcc-sh-elf
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends},
 binutils-sh-elf
Recommends: libnewlib-sh-elf-dev
Suggests:
 gcc-${gcc-version-major}-doc,
 gcc-${gcc-version-major}-locales,
Multi-Arch: foreign
Built-Using: gcc-${gcc-version-major} (= ${gcc-version}), gdb (= ${gdb-version})
Homepage: https://gcc.gnu.org
Description: GNU C compiler for embedded SuperH devices
 This package provides the GNU C compiler targeting bare-metal SuperH
 devices, including all CPUs SH-1 through SH-4a. This package additionally
 provides a simulator, sh-elf-run, that may be used to run the binaries.

Package: libnewlib-sh-elf-dev
Section: libdevel
Architecture: all
Depends: ${misc:Depends}
Suggests: libnewlib-doc
Multi-Arch: foreign
XB-Built-Using-Newlib-Source: ${newlib-source-version}
Homepage: https://www.sourceware.org/newlib/
Description: small ISO C standard library for embedded SuperH devices
 This package provides Newlib, a C library for embedded systems, configured
 for use on SuperH devices.
