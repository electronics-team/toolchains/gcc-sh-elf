/* SPDX-FileCopyrightText: 2021 John Scott <jscott@posteo.net>
 * SPDX-License-Identifier: GPL-3.0-or-later */
#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void) {
	for(size_t i = 0; i < 4; i++) {
		if(fork() == -1) {
			perror("Failed to fork");
			abort();
		}
	}
	if(putchar('a') == EOF) {
		perror("Failed to print character");
		abort();
	}
}
