#define _POSIX_C_SOURCE 200809L
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>

static jmp_buf jmp;
static noreturn void siggy(int s) {
	static int i;
	if(signal(s, siggy) == SIG_ERR) {
		perror("Failed to establish signal handler");
	}
	longjmp(jmp, ++i);
}

int main(void) {
	if(signal(SIGUSR1, siggy) == SIG_ERR) {
		perror("Failed to establish signal handler");
		exit(EXIT_FAILURE);
	}

	volatile int sum = 0;
	int s = setjmp(jmp);
	while(s <= 10) {
		sum += s;
		if(raise(SIGUSR1)) {
			perror("Failed to raise signal");
		}
	}

	if(sum != 10*(10+1)/2) {
		abort();
	}
}
