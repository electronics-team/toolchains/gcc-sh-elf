/* SPDX-FileCopyrightText: 2021 John Scott <jscott@posteo.net>
 * SPDX-License-Identifier: GPL-3.0-or-later */
#define _POSIX_C_SOURCE 200809L
#define _BSD_SOURCE 1
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <wchar.h>

int main(void) {
	time_t now = time(NULL);
	if(now == -1) {
		fputs("Failed to get time\n", stderr);
		abort();
	}

	struct tm *broke_down = localtime(&now);
	if(!broke_down) {
		perror("Failed to represent time");
		abort();
	}

	wchar_t *buf = NULL;
	size_t buflen = 4;
	do {
		buflen *= 2;
		wchar_t *tmp = reallocarray(buf, buflen, sizeof(*tmp));
		if(!tmp) {
			perror("Failed to allocate memory");
			free(buf);
			abort();
		} else {
			buf = tmp;
		}
	} while(!wcsftime(buf, buflen, L"%c\n", broke_down));
	if(fputws(buf, stdout) == -1) {
		perror("Failed to print string");
		free(buf);
		abort();
	}
	free(buf);
}
