#define _XOPEN_SOURCE 700
#include <assert.h>
#include <errno.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <tgmath.h>

int main(void) {
	/* Newlib seems to handle Euler's identity with very high precision. */
	double g = cabs(cexp(2 * M_PI * I) - 1.0);
	static_assert(math_errhandling & MATH_ERRNO, "math errors are not indicated via errno");
	if(errno) {
		perror(NULL);
		abort();
	}
	if(g > 2 * DBL_EPSILON) {
		abort();
	}
}
