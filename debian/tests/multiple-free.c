/* SPDX-FileCopyrightText: 2021 John Scott <jscott@posteo.net>
 * SPDX-License-Identifier: GPL-3.0-or-later */
#include <stdlib.h>

int main(int argc, char *argv[static argc+1]) {
	int (*t)[argc] = malloc(sizeof(*t));
	while(argc-- > 0) {
		free(t);
	}
}
