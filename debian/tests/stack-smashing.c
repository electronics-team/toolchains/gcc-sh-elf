/* SPDX-FileCopyrightText: 2021 John Scott <jscott@posteo.net>
 * SPDX-License-Identifier: GPL-3.0-or-later */
#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <string.h>

int main(void) {
	char arr[1];
	strcpy(arr, "supercalifragilisticexpialidocious");
	if(puts(arr) == EOF) {
		perror("Failed to print string");
	}
}
