/* SPDX-FileCopyrightText: 2021 John Scott <jscott@posteo.net>
 * SPDX-License-Identifier: GPL-3.0-or-later */
#define _POSIX_C_SOURCE 200809L
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[static argc+1]) {
	size_t n = 1;
	for(int i = 1; i < argc; i++) {
		size_t k = strlen(argv[i]) + 1;
		if(!k || n + k < n || n + k < k) {
			errno = EOVERFLOW;
			perror("Failed to concatenate string");
			exit(EXIT_FAILURE);
		}
		n += k;
	}

	char str[n];
	str[0] = '\0';
	char *tmp = str;
	for(int i = 1; i < argc; i++) {
		tmp = stpcpy(tmp, argv[i]);
		if(i != argc - 1) {
			*tmp++ = ' ';
		}
	}
	if(puts(str) == EOF) {
		perror("Failed to print concatenated string");
		exit(EXIT_FAILURE);
	}
}
